package com.itheima.easy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description：项目启动类
 */
@SpringBootApplication
public class TradeWebStart {

    public static void main(String[] args) {
        SpringApplication.run(TradeWebStart.class, args);
    }
}
