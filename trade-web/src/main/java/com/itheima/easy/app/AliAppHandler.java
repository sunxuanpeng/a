package com.itheima.easy.app;

import com.itheima.easy.vo.TradingVo;

public interface AliAppHandler {
    TradingVo getTradingApp(TradingVo tradingVo);
}
