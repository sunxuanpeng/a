package com.itheima.easy.app.Impl;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.app.models.AlipayTradeAppPayResponse;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.itheima.easy.alipay.config.AliPayConfig;
import com.itheima.easy.app.AliAppHandler;
import com.itheima.easy.constant.SuperConstant;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.vo.TradingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AliAppHandlerImpl implements AliAppHandler {
    @Autowired
    private AliPayConfig aliPayConfig;
    @Override
    public TradingVo getTradingApp(TradingVo tradingVo) {
        Factory.setOptions(aliPayConfig.configAliPay());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradeAppPayResponse appPayResponse = Factory.Payment
                    .App().pay(tradingVo.getMemo(),
                            String.valueOf(tradingVo.getTradingOrderNo()),
                            String.valueOf(tradingVo.getTradingAmount()));
            // 3. 处理响应或异常
            if (ResponseChecker.success(appPayResponse)) {
                System.out.println("调用成功");
                tradingVo.setIsRefund(SuperConstant.NO);
                tradingVo.setPlaceOrderCode(TradingConstant.ALI_SUCCESS_CODE);
                tradingVo.setPlaceOrderMsg(TradingConstant.ALI_SUCCESS_MSG);
                //tradeNo.setResultJson(JSONObject.toJSONString(pagePayResponse));
                tradingVo.setPlaceOrderJson(appPayResponse.getBody());
                tradingVo.setTradingState(TradingConstant.TRADE_SUCCESS);
                return tradingVo;
            } else {
                System.err.println("调用失败，原因：" + appPayResponse.getBody() + "，" + appPayResponse.getBody());
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return null;
    }


}
