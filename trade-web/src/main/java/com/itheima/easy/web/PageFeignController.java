package com.itheima.easy.web;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.page.Impl.AliPagePayHandlerImpl;
import com.itheima.easy.vo.TradingVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/trade-common-feign")
@Controller
@Api(tags = "支付基础服务")
public class PageFeignController {

    @Autowired
    private AliPagePayHandlerImpl pagePayHandler;

    //@GetMapping("/payl")
    //@ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=pagePayHandler.getTradingVo1(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }
}
