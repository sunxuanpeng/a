package com.itheima.easy.web;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.itheima.easy.Face.FaceToFace;
import com.itheima.easy.app.Impl.AliAppHandlerImpl;
import com.itheima.easy.handler.CommonPayHandler;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.handler.Face.WechatFaceToFacePayHandler;
import com.itheima.easy.handler.Face.WechatFacetoFace;
import com.itheima.easy.handler.Face.WechatPagePayHandler;
import com.itheima.easy.handler.PagePayHandler;
import com.itheima.easy.page.Impl.AliPagePayHandlerImpl;
import com.itheima.easy.refund.AliRefund;
import com.itheima.easy.vo.RefundRecordVo;
import com.itheima.easy.vo.TradingVo;
import com.itheima.easy.way.AliTrandWay;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName CommonPayController.java
 * @Description 基础支付控制器
 */
@RequestMapping("/trade-common-feign")
@Controller
@Api(tags = "支付基础服务")
public class CommonPayFeignController {

    @Autowired
    CommonPayHandler aliCommonPayHandler;
    @Autowired
    private AliPagePayHandlerImpl pagePayHandler;

    @Autowired
    private AliAppHandlerImpl aliAppHandler;
    @Autowired
    private AliTrandWay aliTrandWay;
    @Autowired
    private AliRefund aliRefund;
    @Autowired
    CommonPayHandler wechatCommonPayHandler;

    @Autowired
    private WechatFaceToFacePayHandler wechatFacetoFace;
    @Autowired
    private FaceToFace faceToFace;
    @Autowired
    private PagePayHandler PagePayHandler;
    /***
     * @description 统一收单线下交易查询
     * 该接口提供所有支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
     * @param tradingVo 交易单
     * @return
     */
    @PostMapping("/query")
    @ResponseBody
    @ApiOperation(value = "支付结果查询",notes = "支付结果查询")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.tradingChannel"})
    TradingVo queryTrading(@RequestBody TradingVo tradingVo){
        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult= aliCommonPayHandler.queryTrading(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult= wechatFacetoFace.precreateTrade(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;
    }
    /***
     * @description 申请退款接口
     * 当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，
     * 将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     * @param tradingVo 交易单
     * @return
     */
    @PostMapping("/refund")
    @ResponseBody
    @ApiOperation(value = "申请退款",notes = "申请退款")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo",
            "tradingVo.operTionRefund","tradingVo.tradingChannel"})
    TradingVo refundTrading(@RequestBody TradingVo tradingVo){
        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult= aliCommonPayHandler.refundTrading(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
          //  tradingVoResult= wechatCommonPayHandler.refundTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;
    }

    /***
     * @description 申请退款查询接口
     * 当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，
     * 将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     * @param refundRecordVo 退款交易单
     * @return
     */
    @PostMapping("/query-refund")
    @ResponseBody
    @ApiOperation(value = "退款结果查询",notes = "退款结果查询")
    @ApiImplicitParam(name = "refundRecordVo",value = "退款交易单",required = true,dataType = "RefundRecordVo")
    @ApiOperationSupport(includeParameters ={"refundRecordVo.tradingChannel",
            "refundRecordVo.tradingOrderNo","refundRecordVo.refundNo"})
    RefundRecordVo queryRefundDownLineTrading(@RequestBody RefundRecordVo refundRecordVo){
        RefundRecordVo refundRecordVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(refundRecordVo.getTradingChannel())){
            refundRecordVoResult= aliCommonPayHandler.queryRefundTrading(refundRecordVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(refundRecordVo.getTradingChannel())){
          //  refundRecordVoResult= wechatCommonPayHandler.queryRefundTrading(refundRecordVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return refundRecordVoResult;
    }

    /***
     * @description 统一关闭订单
     * 1、商户订单支付失败需要生成新单号重新发起支付，要对原订单号调用关单，避免重复支付；
     * 2、系统下单后，用户支付超时，系统退出不再受理，避免用户继续，请调用关单接口。
     * @param tradingVo 退款交易单
     * @return
     */
    @PostMapping("/close-refund")
    @ResponseBody
    @ApiOperation(value = "统一关闭订单",notes = "统一关闭订单")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.tradingChannel"})
    TradingVo closeTrading(@RequestBody TradingVo tradingVo){
        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult = aliCommonPayHandler.closeTrading(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
         //   tradingVoResult= wechatCommonPayHandler.closeTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;
    }

    /***
     * @description 为方便商户快速查账，支持商户通过本接口获取商户离线账单下载地址
     * @param tradingVo 退款交易单
     * @return
     */
    @PostMapping("/down-load-bill")
    @ResponseBody
    @ApiOperation(value = "下载交易单",notes = "下载交易单")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.billType","tradingVo.billDate","tradingVo.tradingChannel"})
    TradingVo downLoadBill(@RequestBody TradingVo tradingVo){
        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult= aliCommonPayHandler.downLoadBill(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
         //   tradingVoResult= wechatCommonPayHandler.downLoadBill(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;
    }
    @PostMapping("/precreate")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo precreate(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=faceToFace.getTradingVo(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }
    @PostMapping("/payl")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=faceToFace.getTradingVo1(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;
    }

    @PostMapping("/payl04")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay04(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=pagePayHandler.getTradingVo1(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }

    @PostMapping("/trade-app-feign")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay05(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=aliAppHandler.getTradingApp(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }

    @PostMapping("/trade-way-feign")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay06(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=aliTrandWay.getTradingWay(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }

    @PostMapping("/trade-refund-feign")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo pay07(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=aliRefund.refundTrading(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
            // TODO tradingVoResult= wechatCommonPayHandler.queryTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }
    @PostMapping("/page-trading")
    @ResponseBody
    @ApiOperation(value = "用户扫商家二维码",notes = "用户扫商家二维码")
    @ApiImplicitParam(name = "tradingVo",value = "交易单",required = true,dataType = "TradingVo")
    @ApiOperationSupport(includeParameters ={"tradingVo.tradingOrderNo","tradingVo.memo","tradingVo.tradingAmount","tradingVo.tradingChannel"})
    public TradingVo Pay08(@RequestBody TradingVo tradingVo){

        TradingVo tradingVoResult = null;
        if (TradingConstant.TRADING_CHANNEL_ALI_PAY.equals(tradingVo.getTradingChannel())){
            tradingVoResult=pagePayHandler.getTradingVo1(tradingVo);
        }else if (TradingConstant.TRADING_CHANNEL_WECHAT_PAY.equals(tradingVo.getTradingChannel())){
              tradingVoResult= PagePayHandler.pageTrading(tradingVo);
        }else {
            throw new RuntimeException("请输入渠道！");
        }
        return tradingVoResult;

    }
}
