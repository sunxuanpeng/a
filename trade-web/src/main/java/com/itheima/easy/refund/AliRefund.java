package com.itheima.easy.refund;

import com.itheima.easy.vo.RefundRecordVo;
import com.itheima.easy.vo.TradingVo;

public interface AliRefund {
    TradingVo queryTrading(TradingVo tradingVo);
    TradingVo refundTrading(TradingVo tradingVo);
    RefundRecordVo queryRefundTrading(RefundRecordVo refundRecordVo);
    TradingVo downLoadBill(TradingVo tradingVo);
    public TradingVo closeTrading(TradingVo tradingVo);
}
