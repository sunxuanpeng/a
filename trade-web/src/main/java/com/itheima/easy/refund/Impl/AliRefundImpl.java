package com.itheima.easy.refund.Impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.common.models.*;
import com.alipay.easysdk.payment.wap.models.AlipayTradeWapPayResponse;
import com.itheima.easy.alipay.config.AliPayConfig;
import com.itheima.easy.constant.SuperConstant;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.enums.TradingEnum;
import com.itheima.easy.exception.ProjectException;
import com.itheima.easy.refund.AliRefund;
import com.itheima.easy.utils.BeanConv;
import com.itheima.easy.utils.EmptyUtil;
import com.itheima.easy.utils.ExceptionsUtil;
import com.itheima.easy.utils.SnowflakeIdWorker;
import com.itheima.easy.vo.RefundRecordVo;
import com.itheima.easy.vo.TradingVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AliRefundImpl implements AliRefund  {

    @Autowired
    AliPayConfig aliPayConfig;

    @Override
    public TradingVo queryTrading(TradingVo tradingVo) {
        //1、获得支付宝配置文件
        Config config = aliPayConfig.configAliPay();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new ProjectException(TradingEnum.CONFIG_ERROR);
        }
        //3、使用配置
        Factory.setOptions(config);
        try {
            //4、调用支付宝API：通用查询支付情况
            AlipayTradeQueryResponse queryResponse = Factory.Payment.Common()
                    .query(String.valueOf(tradingVo.getTradingOrderNo()));
            //5、判断响应是否成功
            boolean success = ResponseChecker.success(queryResponse);
            //6、响应成功，分析交易状态
            boolean flag = true;
            if (success&&!EmptyUtil.isNullOrEmpty(queryResponse.getTradeStatus())){
                switch (queryResponse.getTradeStatus()){
                    //支付取消：TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）
                    case TradingConstant.ALI_TRADE_CLOSED:
                        tradingVo.setTradingState(TradingConstant.TRADE_CLOSED);break;
                    //支付成功：TRADE_SUCCESS（交易支付成功）
                    case TradingConstant.ALI_TRADE_SUCCESS:
                        tradingVo.setTradingState(TradingConstant.TRADE_SUCCESS);break;
                    //支付成功：TRADE_FINISHED（交易结束，不可退款）
                    case TradingConstant.ALI_TRADE_FINISHED:
                        tradingVo.setTradingState(TradingConstant.TRADE_SUCCESS);break;
                    //非最终状态不处理，当前交易状态：WAIT_BUYER_PAY（交易创建，等待买家付款）不处理
                    default:
                        flag = false;break;
                }
                //7、修改交易单状态
                if (flag){
                    tradingVo.setResultCode(queryResponse.getSubCode());
                    tradingVo.setResultMsg(queryResponse.getSubMsg());
                    tradingVo.setResultJson(JSONObject.toJSONString(queryResponse));
                    return tradingVo;
                }else {
                    log.info("查询支付宝交易单：{},结果：{}", tradingVo.getTradingOrderNo(),queryResponse.getTradeStatus());
                }
            }else {
                throw new RuntimeException("网关：查询支付宝统一下单失败！");
            }
        } catch (Exception e) {
            log.warn("查询支付宝统一下单失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("查询支付宝统一下单失败！");
        }
        return tradingVo;
    }

    @Override
    public TradingVo refundTrading(TradingVo tradingVo) {
        //1、生成退款请求编号
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1,1);
        tradingVo.setOutRequestNo(String.valueOf(snowflakeIdWorker.nextId()));
        //2、获得支付宝配置文件
        Config config = aliPayConfig.configAliPay();
        //3、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new ProjectException(TradingEnum.CONFIG_ERROR);
        }
        //4、使用配置
        Factory.setOptions(config);
        //5、调用退款接口
        try {
            AlipayTradeRefundResponse refundResponse = Factory.Payment.Common()
                    .optional("out_request_no", tradingVo.getOutRequestNo())
                    .refund(String.valueOf(tradingVo.getTradingOrderNo()),
                            String.valueOf(tradingVo.getOperTionRefund()));
            //6、判断响应是否成功
            boolean success = ResponseChecker.success(refundResponse);
            if (success&&String.valueOf(tradingVo.getTradingOrderNo()).equals(refundResponse.getOutTradeNo())){
                //7、保存交易单信息
                tradingVo.setIsRefund(SuperConstant.YES);
                //支持累计退款！
                tradingVo.setRefund(tradingVo.getRefund().add(tradingVo.getOperTionRefund()));
                //8、保存退款单信息
                RefundRecordVo refundRecordVo = RefundRecordVo.builder()
                        .enterpriseId(tradingVo.getEnterpriseId())
                        .storeId(tradingVo.getStoreId())
                        .refundNo(tradingVo.getOutRequestNo())
                        .refundAmount(tradingVo.getOperTionRefund())
                        .refundCode(refundResponse.getSubCode())
                        .refundMsg(refundResponse.getSubMsg())
                        .productOrderNo(tradingVo.getProductOrderNo())
                        .tradingChannel(tradingVo.getTradingChannel())
                        .tradingOrderNo(tradingVo.getTradingOrderNo())
                        .refundStatus(success?TradingConstant.REFUND_STATUS_SENDING:TradingConstant.REFUND_STATUS_FAIL)
                        .memo("退款："+tradingVo.getMemo())
                        .build();
            }else {
                log.error("网关：支付宝统一下单退款：{},结果：{}", tradingVo.getTradingOrderNo(),
                        JSONObject.toJSONString(refundResponse));
                throw new RuntimeException("网关：支付宝统一下单退款失败");
            }
        } catch (Exception e) {
            log.error("支付宝统一下单退款失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new ProjectException(TradingEnum.TRAD_REFUND_FAIL);
        }
        return BeanConv.toBean(tradingVo,TradingVo.class);
    }

    @Override
    public RefundRecordVo queryRefundTrading(RefundRecordVo refundRecordVo) {
        //1、获得支付宝配置文件
        Config config = aliPayConfig.configAliPay();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new ProjectException(TradingEnum.CONFIG_ERROR);
        }
        //3、使用配置
        Factory.setOptions(config);
        try {
            AlipayTradeFastpayRefundQueryResponse refundQueryResponse =
                    Factory.Payment.Common().queryRefund(
                            String.valueOf(refundRecordVo.getTradingOrderNo()),
                            refundRecordVo.getRefundNo());
            //4、判断响应是否成功
            boolean success = ResponseChecker.success(refundQueryResponse);
            //5、查询出的退款状态
            String refundStatus = refundQueryResponse.getRefundStatus();
            if (success&&TradingConstant.REFUND_SUCCESS.equals(refundStatus)){
                refundRecordVo.setRefundStatus(TradingConstant.REFUND_STATUS_SUCCESS);
                refundRecordVo.setRefundCode(refundQueryResponse.getSubCode());
                refundRecordVo.setRefundMsg(refundQueryResponse.getSubMsg());
            }else {
                log.error("网关：查询支付宝统一下单退款：{},结果：{}", refundRecordVo.getTradingOrderNo(),
                        JSONObject.toJSONString(refundQueryResponse));
                throw new RuntimeException("网关：查询支付宝统一下单退款失败！");
            }
        } catch (Exception e) {
            log.warn("查询支付宝统一下单退款失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("查询支付宝统一下单退款失败！");
        }
        return refundRecordVo;
    }

    @Override
    public TradingVo closeTrading(TradingVo tradingVo) {
        //1、获得支付宝配置文件
        Config config = aliPayConfig.configAliPay();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new ProjectException(TradingEnum.CONFIG_ERROR);
        }
        //3、使用配置
        Factory.setOptions(config);
        try {
            //4、调用三方API关闭订单
            AlipayTradeCloseResponse closeResponse = Factory.Payment.Common()
                    .close(String.valueOf(tradingVo.getTradingOrderNo()));
            //5、关闭订单受理情况
            boolean success = ResponseChecker.success(closeResponse);
            if (success&&tradingVo.getTradingOrderNo().equals(closeResponse.getOutTradeNo())){
                tradingVo.setTradingState(TradingConstant.TRADE_CLOSED);
                return tradingVo;
            }else {
                log.error("网关：支付宝关闭订单：{},结果：{}", tradingVo.getTradingOrderNo(),
                        JSONObject.toJSONString(closeResponse));
                throw  new RuntimeException("网关：支付宝关闭订单失败!");
            }
        } catch (Exception e) {
            log.warn("关闭订单失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw  new ProjectException(TradingEnum.TRAD_CLOSE_FAIL);
        }
    }

    @Override
    public TradingVo downLoadBill(TradingVo tradingVo) {
        //1、获得支付宝配置文件
        Config config = aliPayConfig.configAliPay();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new ProjectException(TradingEnum.CONFIG_ERROR);
        }
        //3、使用配置
        Factory.setOptions(config);
        try {
            AlipayDataDataserviceBillDownloadurlQueryResponse billDownloadurlQueryResponse = Factory.Payment.Common()
                    .downloadBill(tradingVo.getBillType(), DateUtil.formatDate(tradingVo.getBillDate()));
            //4、请求下载受理情况
            boolean success = ResponseChecker.success(billDownloadurlQueryResponse);
            if (success){
                String billDownloadUrl = billDownloadurlQueryResponse.getBillDownloadUrl();
                tradingVo.setBillDownloadUrl(billDownloadUrl);
            }else {
                log.warn("网关：支付宝下载订单类型：{},时间：{},结果：{}", tradingVo.getBillType(),tradingVo.getBillDate(),
                        JSONObject.toJSONString(billDownloadurlQueryResponse));
                throw new RuntimeException("网关：支付宝下载订单失败！");
            }
        } catch (Exception e) {
            log.warn("支付宝下载订单失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw  new RuntimeException("支付宝下载订单失败！");
        }
        return tradingVo;
    }

}