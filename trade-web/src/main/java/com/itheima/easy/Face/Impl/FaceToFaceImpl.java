package com.itheima.easy.Face.Impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePayResponse;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePrecreateResponse;
import com.itheima.easy.Face.FaceToFace;
import com.itheima.easy.alipay.config.AliPayConfig;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.utils.QrcodeUtil;
import com.itheima.easy.vo.TradingVo;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.commons.compiler.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FaceToFaceImpl implements FaceToFace {
    @Autowired
    private AliPayConfig aliPayConfig;

    @Override
    public TradingVo getTradingVo(TradingVo tradeNo) {

        Factory.setOptions(aliPayConfig.configAliPay());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradePrecreateResponse response = Factory.Payment.FaceToFace()
                    .preCreate(tradeNo.getMemo(), String.valueOf(tradeNo.getTradingOrderNo()), String.valueOf(tradeNo.getTradingAmount()));
            // 3. 处理响应或异常
            if (ResponseChecker.success(response)) {
                System.out.println("调用成功");
                tradeNo.setPlaceOrderCode(response.getCode());
                tradeNo.setPlaceOrderMsg(response.getMsg());
                //tradeNo.setPlaceOrderJson(response.getQrCode());
                if (StringUtils.isNotBlank(response.getQrCode())){
                    String imageBase64= QrcodeUtil.generatePng(response.getQrCode(),200,200);
                    tradeNo.setPlaceOrderJson(imageBase64);
                }

                return tradeNo;
            } else {
                System.err.println("调用失败，原因：" + response.msg + "，" + response.subMsg);
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public TradingVo getTradingVo1(TradingVo tradeNo) {
        Factory.setOptions(aliPayConfig.configAliPay());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradePayResponse response = Factory.Payment.FaceToFace()
                    .pay(tradeNo.getMemo(),
                            String.valueOf(tradeNo.getTradingOrderNo()),
                            String.valueOf(tradeNo.getTradingAmount()),
                            tradeNo.getAuthCode()
                    );
            // 3. 处理响应或异常
            if (ResponseChecker.success(response)) {
                System.out.println("调用成功");

                tradeNo.setPlaceOrderCode(response.getCode());
                tradeNo.setPlaceOrderMsg(response.getMsg());
                tradeNo.setResultJson(JSONObject.toJSONString(response));
                tradeNo.setTradingState(TradingConstant.TRADE_SUCCESS);

                //tradeNo.setPlaceOrderJson(response.getAuthTradePayMode());
                //tradeNo.setTradingState();
                /*if (StringUtils.isNotBlank(response.getAuthTradePayMode())){
                    String imageBase64= QrcodeUtil.generateSvg(response.getAuthTradePayMode(),200,200);
                    tradeNo.setPlaceOrderJson(imageBase64);
                }*/

                return tradeNo;
            } else {
                System.err.println("调用失败，原因：" + response.msg + "，" + response.subMsg);
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return null;
    }




}
