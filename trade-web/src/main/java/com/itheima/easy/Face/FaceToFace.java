package com.itheima.easy.Face;

import com.itheima.easy.vo.TradingVo;
import org.springframework.stereotype.Service;


public interface FaceToFace {

    public TradingVo getTradingVo(TradingVo tradeNo);

    public TradingVo getTradingVo1(TradingVo tradeNo);


}
