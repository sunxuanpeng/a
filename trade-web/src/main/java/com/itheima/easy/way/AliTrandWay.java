package com.itheima.easy.way;

import com.itheima.easy.vo.TradingVo;

public interface AliTrandWay {
    TradingVo getTradingWay(TradingVo tradingVo);
}
