package com.itheima.easy.way.Impl;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.alipay.easysdk.payment.wap.models.AlipayTradeWapPayResponse;
import com.itheima.easy.alipay.config.AliPayConfig;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.vo.TradingVo;
import com.itheima.easy.way.AliTrandWay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AliTrandWayImpl implements AliTrandWay {


    @Autowired
    private AliPayConfig aliPayConfig;
    @Override
    public TradingVo getTradingWay(TradingVo tradeNo) {
        Factory.setOptions(aliPayConfig.configAliPay());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradeWapPayResponse wapPayResponse = Factory.Payment.Wap()
                    .pay(tradeNo.getMemo(),String.valueOf(tradeNo.getTradingOrderNo()),
                            String.valueOf(tradeNo.getTradingAmount()), tradeNo.getQuitUrl(), tradeNo.getReturnUrl());
            //6、检查网关响应结果
            // 3. 处理响应或异常
            if (ResponseChecker.success(wapPayResponse)) {
                System.out.println("调用成功");

                tradeNo.setPlaceOrderCode(TradingConstant.ALI_SUCCESS_CODE);
                tradeNo.setPlaceOrderMsg(TradingConstant.ALI_SUCCESS_MSG);
                //tradeNo.setResultJson(JSONObject.toJSONString(pagePayResponse));
                tradeNo.setPlaceOrderJson(wapPayResponse.getBody());
                tradeNo.setTradingState(TradingConstant.TRADE_SUCCESS);
                return tradeNo;
            } else {
                System.err.println("调用失败，原因：" + wapPayResponse.getBody() + "，" + wapPayResponse.getBody());
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return null;
    }

}
