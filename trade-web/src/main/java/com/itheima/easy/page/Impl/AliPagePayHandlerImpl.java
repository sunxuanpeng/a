package com.itheima.easy.page.Impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePayResponse;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.itheima.easy.alipay.config.AliPayConfig;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.handler.common.AliCommonPayHandler;
import com.itheima.easy.page.AliPagePayHandler;
import com.itheima.easy.vo.TradingVo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AliPagePayHandlerImpl extends AliCommonPayHandler implements AliPagePayHandler{


    @Autowired
    private AliPayConfig aliPayConfig;
    @Override
    public TradingVo getTradingVo1(TradingVo tradeNo) {
        Factory.setOptions(aliPayConfig.configAliPay());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradePagePayResponse pagePayResponse = Factory.Payment.Page()
                    .pay(tradeNo.getMemo(),String.valueOf(tradeNo.getTradingOrderNo()),
                            String.valueOf(tradeNo.getTradingAmount()),
                            tradeNo.getReturnUrl());
            // 3. 处理响应或异常
            if (ResponseChecker.success(pagePayResponse)) {
                System.out.println("调用成功");

                tradeNo.setPlaceOrderCode(TradingConstant.ALI_SUCCESS_CODE);
                tradeNo.setPlaceOrderMsg(TradingConstant.ALI_SUCCESS_MSG);
                //tradeNo.setResultJson(JSONObject.toJSONString(pagePayResponse));
                tradeNo.setPlaceOrderJson(pagePayResponse.getBody());
                tradeNo.setTradingState(TradingConstant.TRADE_SUCCESS);
                return tradeNo;
            } else {
                System.err.println("调用失败，原因：" + pagePayResponse.getBody() + "，" + pagePayResponse.getBody());
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return null;
    }



}
