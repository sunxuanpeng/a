package com.itheima.easy.utils;

import com.itheima.easy.basic.IBasicEnum;
import com.itheima.easy.basic.ResponseWrap;
import com.itheima.easy.enums.BasicEnum;

import java.util.Date;

/**
 * @Description 构造ResponseWrap工具
 */
public class ResponseWrapBuild {

    public static <T> ResponseWrap<T> build(IBasicEnum basicEnumIntface, T t){

         //构建对象
        return ResponseWrap.<T>builder()
            .code(basicEnumIntface.getCode())
            .msg(basicEnumIntface.getMsg())
            .operationTime(new Date())
            .datas(t)
            .build();
    }

    /**
     * 公共成功响应方法
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseWrap<T> successBuild(T t){
        return ResponseWrapBuild.build(BasicEnum.SUCCEED,t);
    }

    /**
     * 公共失败响应方法
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseWrap<T> failedBuild(T t){
        return ResponseWrapBuild.build(BasicEnum.SYSYTEM_FAIL,t);
    }

}
