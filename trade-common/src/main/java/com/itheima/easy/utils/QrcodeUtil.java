package com.itheima.easy.utils;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * @Description: 二维码生成工具
 * @author: nianqiang
 * @Version: V1.0
 */
public class QrcodeUtil {

		public static void main(String[] args) {
				String generate = QrcodeUtil.generatePng("http://www.itcast.cn", 200, 200);
				System.out.println(generate);
		}
		/**
		 * <p>生成二维码- JPEG 格式</p>
		 * @param content 二维码内容
		 * @param width 二维码宽度
		 * @param height 二维码高度
		 * @return BASE64编码格式, 浏览器可以或<image/> 直接渲染
		 */
		public static String generateJpg(String content, int width, int height) {
				QrConfig qrConfig = new QrConfig(width, height);
				return QrCodeUtil.generateAsBase64(content, qrConfig, ImageType.JPEG.fileExtension);
		}

		/**
		 * <p>生成二维码- SVG 格式</p>
		 * @param content 二维码内容
		 * @param width 二维码宽度
		 * @param height 二维码高度
		 * @return BASE64编码格式, 浏览器可以或<image/> 直接渲染
		 */
		public static String generateSvg(String content, int width, int height) {
				QrConfig qrConfig = new QrConfig(width, height);
				return QrCodeUtil.generateAsBase64(content, qrConfig, ImageType.SVG.fileExtension);
		}
		/**
		 * <p>生成二维码- PNG 格式</p>
		 * @param content 二维码内容
		 * @param width 二维码宽度
		 * @param height 二维码高度
		 * @return BASE64编码格式, 浏览器可以或<image/> 直接渲染
		 */
		public static String generatePng(String content, int width, int height) {
				QrConfig qrConfig = new QrConfig(width, height);
				return QrCodeUtil.generateAsBase64(content, qrConfig, ImageType.PNG.fileExtension);
		}

		/**
		 * <p>生成二维码文件</p>
		 * @param content 二维码内容
		 * @param width 二维码宽度
		 * @param height 二维码高度
		 * @throws FileNotFoundException
		 */
		public static void generate(String content, int width, int height, String path) throws FileNotFoundException {
				QrConfig qrConfig = new QrConfig(width, height);
				QrCodeUtil.generate(content, qrConfig,
								ImageType.JPEG.fileExtension, new FileOutputStream(new File(path)));
		}

		/**
		 * <p>生成二维码文件</p>
		 * @param content 二维码内容
		 * @param width 二维码宽度
		 * @param height 二维码高度
		 * @param path 保存本地路径文件位置
		 * @return 文件对象
		 * @throws FileNotFoundException
		 */
		public static File generateFile(String content, int width, int height, String path) throws FileNotFoundException {
				QrConfig qrConfig = new QrConfig(width, height);
				return QrCodeUtil.generate(content, qrConfig, new File(path));
		}

		public enum ImageType {
				JPEG("jpg"),
				TXT("txt"),
				SVG("svg"),
				PNG("png"),
				GIF("gif"),
				BMP("bmp");

				private final String fileExtension;

				ImageType(String fileExtension) {
						this.fileExtension = fileExtension;
				}

				public String getFileExtension() {
						return fileExtension;
				}
		}

}
