package com.itheima.easy.alipay.config;

import com.alipay.easysdk.kernel.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 支付宝配置类
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(AliPayProperties.class)
public class AliPayConfig {

    @Autowired
    AliPayProperties aliPayProperties;

    /***
     * @description 获得配置
     * @return
     */
    @Bean
    public Config configAliPay(){
        //1、创建配置
        Config config = new Config();
        config.protocol = aliPayProperties.getProtocol();
        config.gatewayHost =aliPayProperties.getGatewayHost();
        config.signType = aliPayProperties.getSignType();
        config.appId = aliPayProperties.getAppId();
        //2、配置应用私钥
        config.merchantPrivateKey = aliPayProperties.getMerchantPrivateKey();
        //3、配置支付宝公钥
        config.alipayPublicKey = aliPayProperties.getAlipayPublicKey();
        //4、可设置异步通知接收服务地址（可选）
        config.notifyUrl = aliPayProperties.getNotifyUrl();
        //5、设置AES密钥，调用AES加解密相关接口时需要（可选）
        config.encryptKey = aliPayProperties.getEncryptKey();
        return config;
    }
}
