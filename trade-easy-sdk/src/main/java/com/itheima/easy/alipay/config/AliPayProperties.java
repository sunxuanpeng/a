package com.itheima.easy.alipay.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName AlipayProperties.java
 * @Description 阿里云支付配置
 */
@Data
@ConfigurationProperties(prefix = "itheima.trade.alipay")
public class AliPayProperties {

    //网关host
    public String gatewayHost;

    //协议
    public String protocol ="https";

    //签名：默认RSA2
    public String signType="RSA2";

    //应用Id
    public String appId;

    //应用私钥
    public String merchantPrivateKey;

    //支付宝公钥
    public String alipayPublicKey;

    //可设置异步通知接收服务地址（可选）
    public String notifyUrl;

    //设置AES密钥，调用AES加解密相关接口时需要（可选）
    public String encryptKey;
}
