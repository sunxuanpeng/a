package com.itheima.easy.handler;

import com.itheima.easy.vo.TradingVo;

/**
 * @ClassName FaceToFacePayHandler.java
 * @Description 扫码付支付接口
 */
public interface FaceToFacePayHandler extends CommonPayHandler {



    /***
     * @description 生成交易付款码，待用户扫码付款
     * @param tradingVo 订单
     * @return  二维码路径
     */
    TradingVo precreateTrade(TradingVo tradingVo);
}