package com.itheima.easy.handler.Face;

import com.alibaba.fastjson.JSONObject;
import com.itheima.easy.constant.SuperConstant;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.handler.FaceToFacePayHandler;
import com.itheima.easy.handler.common.WechatCommonPayHandler;
import com.itheima.easy.utils.EmptyUtil;
import com.itheima.easy.utils.ExceptionsUtil;
import com.itheima.easy.vo.RefundRecordVo;
import com.itheima.easy.vo.TradingVo;
import com.itheima.easy.wechat.client.Config;
import com.itheima.easy.wechat.client.Factory;

import com.itheima.easy.wechat.config.WechatPayConfig;
import com.itheima.easy.wechat.response.PreCreateResponse;
import com.itheima.easy.wechat.utils.ResponseChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName WechatNativePayHandler.java
 * @Description Native支付方式：商户生成二维码，用户扫描支付
 */
@Slf4j
@Component
public class WechatFaceToFacePayHandler extends WechatCommonPayHandler implements FaceToFacePayHandler {

    @Autowired
    private WechatPayConfig wechatPayConfig;

    @Override
    public TradingVo precreateTrade(TradingVo tradingVo) {
        //1、获得微信客户端
        Config config = wechatPayConfig.config();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)){
            throw new RuntimeException("微信支付配置为空！");
        }
        //3、使用配置
        Factory.setOptions(config);
        try {
            //4、调用接口
            PreCreateResponse preCreateResponse = Factory.FaceToFace().precreate(
                String.valueOf(tradingVo.getTradingOrderNo()),
                String.valueOf(tradingVo.getTradingAmount()),
                tradingVo.getMemo());
            //7、受理结果【只表示请求是否成功，而不是支付是否成功】
            boolean success = ResponseChecker.success(preCreateResponse);
            //8、受理成功：修改交易单
            if (success&&!EmptyUtil.isNullOrEmpty(preCreateResponse.getCodeUrl())){
                tradingVo.setIsRefund(SuperConstant.NO);
                tradingVo.setPlaceOrderCode(preCreateResponse.getCode());
                tradingVo.setPlaceOrderMsg(preCreateResponse.getMessage());
                tradingVo.setPlaceOrderJson(preCreateResponse.getCodeUrl());
                tradingVo.setTradingState(TradingConstant.TRADE_WAIT_BUYER_PAY);
            }else {
                log.error("网关：precreateResponse：{},结果：{}", tradingVo.getTradingOrderNo(),
                        JSONObject.toJSONString(preCreateResponse));
                throw new RuntimeException("网关：微信商家扫用户统一下单创建失败！");
            }
        } catch (Exception e) {
            log.error("微信用户扫商家统一下单创建失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("微信商家扫用户统一下单创建失败！");
        }
        return tradingVo;
    }

    /**
     * TODO V3版本暂不支持改功能
     * @param tradingVo 订单
     * @return
     */


    @Override
    public TradingVo queryTrading(TradingVo tradingVo) {
        return null;
    }

    @Override
    public TradingVo refundTrading(TradingVo tradingVo) {
        return null;
    }

    @Override
    public RefundRecordVo queryRefundTrading(RefundRecordVo refundRecordVo) {
        return null;
    }

    @Override
    public TradingVo closeTrading(TradingVo tradingVo) {
        return null;
    }

    @Override
    public TradingVo downLoadBill(TradingVo tradingVo) {
        return null;
    }
}