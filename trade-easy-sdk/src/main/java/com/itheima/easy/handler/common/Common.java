package com.itheima.easy.handler.common;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.itheima.easy.utils.EmptyUtil;
import com.itheima.easy.wechat.client.Config;
import com.itheima.easy.wechat.client.WechatPayHttpClient;
import com.itheima.easy.wechat.response.CloseResponse;
import com.itheima.easy.wechat.response.QueryResponse;
import com.itheima.easy.wechat.response.RefundResponse;
import com.itheima.easy.wechat.response.TradebillResponse;

import java.math.BigDecimal;

/**
 * 支付基础功能
 *
 * @Author itcast
 * @Create 2023/6/2
 **/
public class Common {
    private Config config;

    public Common(Config config){
        this.config = config;
    }

    /***
     * @description 查询交易结果
     * @param outTradeNo 发起支付传递的交易单号(订单号)
     * @return
     */
    public QueryResponse query(String outTradeNo) throws Exception {
        //请求地址
        String uri ="/v3/pay/transactions/out-trade-no";

        //构建请求对象
        WechatPayHttpClient httpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .mchSerialNo(config.getMchSerialNo())
                .apiV3Key(config.getApiV3Key())
                .privateKey(config.getPrivateKey())
                .domain(config.getDomain()+uri)
                .build();

        //封装参数
        String uriParams ="/"+outTradeNo+"?mchid="+config.getMchId();

        //发送请求
        String body =  httpClient.doGet(uriParams);

        //解析返回结果
        QueryResponse queryResponse = JSONObject.parseObject(body, QueryResponse.class);
        if (!EmptyUtil.isNullOrEmpty(queryResponse.getTradeState())){
            queryResponse.setCode("200");
            queryResponse.setMessage("网关请求成功");
        }
        return queryResponse;
    }

    /***
     * @description 统一收单交易退款接口
     * @param outTradeNo 发起支付传递的交易单号
     * @param amount 退款金额
     * @param outRefundNo 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔
     * @param total 原订单金额
     * @return
     */
    public RefundResponse refund(String outTradeNo, String amount, String outRefundNo, String total) throws Exception {
        //请求地址
        String uri ="/v3/refund/domestic/refunds";
        WechatPayHttpClient httpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .mchSerialNo(config.getMchSerialNo())
                .apiV3Key(config.getApiV3Key())
                .privateKey(config.getPrivateKey())
                .domain(config.getDomain()+uri)
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode bodyParams = objectMapper.createObjectNode();
        BigDecimal bigDecimal = new BigDecimal(amount);
        BigDecimal multiply = bigDecimal.multiply(new BigDecimal(100));
        BigDecimal bigDecimalTotal = new BigDecimal(total);
        BigDecimal multiplyTotal = bigDecimalTotal.multiply(new BigDecimal(100));
        bodyParams.put("out_refund_no", outRefundNo)
                .put("out_trade_no", outTradeNo);
        bodyParams.putObject("amount")
                .put("refund", multiply.intValue())
                .put("total", multiplyTotal.intValue())
                .put("currency", "CNY");
        String body =  httpClient.doPost(bodyParams);
        RefundResponse refundResponse = JSONObject.parseObject(body, RefundResponse.class);
        if (!EmptyUtil.isNullOrEmpty(refundResponse.getStatus())){
            refundResponse.setCode("200");
            refundResponse.setMessage("网关请求成功");
        }
        return refundResponse;
    }

    /***
     * @description 统一收单交易退款接口查询
     * @param outRefundNo 商户系统内部的退款单号
     * @return
     */
    public RefundResponse queryRefund(String outRefundNo) throws Exception {
        //请求地址
        String uri ="/v3/refund/domestic/refunds";
        WechatPayHttpClient httpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .mchSerialNo(config.getMchSerialNo())
                .apiV3Key(config.getApiV3Key())
                .privateKey(config.getPrivateKey())
                .domain(config.getDomain()+uri)
                .build();
        String uriParams ="/"+outRefundNo;
        String body =  httpClient.doGet(uriParams);
        RefundResponse refundResponse = JSONObject.parseObject(body, RefundResponse.class);
        if (!EmptyUtil.isNullOrEmpty(refundResponse.getStatus())){
            refundResponse.setCode("200");
            refundResponse.setMessage("网关请求成功");
        }
        return refundResponse;
    }

    /***
     * @description 统一关闭订单
     * 1、商户订单支付失败需要生成新单号重新发起支付，要对原订单号调用关单，避免重复支付；
     * 2、系统下单后，用户支付超时，系统退出不再受理，避免用户继续，请调用关单接口。
     * @param outTradeNo 外部交易单号
     * @return
     */
    public CloseResponse close(String outTradeNo) throws Exception {
        //请求地址
        String uri ="/v3/pay/transactions/out-trade-no/"+outTradeNo+"/close";
        WechatPayHttpClient httpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .mchSerialNo(config.getMchSerialNo())
                .apiV3Key(config.getApiV3Key())
                .privateKey(config.getPrivateKey())
                .domain(config.getDomain()+uri)
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode bodyParams = objectMapper.createObjectNode();
        bodyParams.put("mchid", config.getMchId());
        httpClient.doPost(bodyParams);
        CloseResponse closeResponse = new CloseResponse();
        closeResponse.setCode("200");
        closeResponse.setMessage("网关请求成功");
        return closeResponse;
    }

    /**
     * 下载交易单
     * @param billDate
     * @return
     * @throws Exception
     */
    public TradebillResponse downLoadTradebill(String billDate) throws Exception{
        //微信接口地址
        String uri ="/v3/bill/tradebill?bill_date=" + billDate;
        //创建微信支付http客户端对象
        WechatPayHttpClient httpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .mchSerialNo(config.getMchSerialNo())
                .apiV3Key(config.getApiV3Key())
                .privateKey(config.getPrivateKey())
                .domain(config.getDomain()+uri)
                .build();

        String body = httpClient.doGet("");

        TradebillResponse response = JSONObject.parseObject(body, TradebillResponse.class);

        if (!EmptyUtil.isNullOrEmpty(response.getDownloadUrl())){
            response.setCode("200");
            response.setMessage("网关请求成功");
            response.setSubCode("10000");
            response.setSubMessage("交易账单下载成功");
        }
        return response;
    }
}
