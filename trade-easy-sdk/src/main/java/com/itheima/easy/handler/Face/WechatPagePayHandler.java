package com.itheima.easy.handler.Face;

import com.alibaba.fastjson.JSONObject;
import com.itheima.easy.constant.SuperConstant;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.handler.PagePayHandler;
import com.itheima.easy.handler.common.WechatCommonPayHandler;
import com.itheima.easy.utils.EmptyUtil;
import com.itheima.easy.utils.ExceptionsUtil;
import com.itheima.easy.vo.TradingVo;
import com.itheima.easy.wechat.client.Config;
import com.itheima.easy.wechat.client.Factory;
import com.itheima.easy.wechat.config.WechatPayConfig;

import com.itheima.easy.wechat.response.PagePayResponse;
import com.itheima.easy.wechat.utils.ResponseChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName WechatPagePayHandler.java
 * @Description PC网页支付处理实现
 */
@Slf4j
@Component
public class WechatPagePayHandler extends WechatCommonPayHandler implements PagePayHandler {

    @Autowired
    private WechatPayConfig wechatPayConfig;

    @Override
    public TradingVo pageTrading(TradingVo tradingVo) {
        //1、获得微信客户端
        Config config = wechatPayConfig.config();
        //2、配置如果为空，抛出异常
        if (EmptyUtil.isNullOrEmpty(config)) {
            throw new RuntimeException("微信支付配置为空！");
        }
        //3、指定配置文件
        Factory.setOptions(config);
        try {
            //4、调用接口
            PagePayResponse pagePayResponse = Factory.Page().pay(
                    String.valueOf(tradingVo.getTradingOrderNo()),
                       String.valueOf(tradingVo.getTradingAmount()),
                     tradingVo.getMemo(),
                               tradingVo.getOpenId());
            //5、受理结果【只表示请求是否成功，而不是支付是否成功】
            boolean success = ResponseChecker.success(pagePayResponse);
            if (success && !EmptyUtil.isNullOrEmpty(pagePayResponse.getPrepayId())) {
                tradingVo.setIsRefund(SuperConstant.NO);
                tradingVo.setPlaceOrderCode(pagePayResponse.getCode());
                tradingVo.setPlaceOrderMsg(pagePayResponse.getMessage());
                tradingVo.setPlaceOrderJson(pagePayResponse.getPrepayId());
                tradingVo.setTradingState(TradingConstant.TRADE_WAIT_BUYER_PAY);
            } else {
                log.error("网关：微信Page支付preCreate：{},结果：{}", tradingVo.getTradingOrderNo(), JSONObject.toJSONString(pagePayResponse));
                throw new RuntimeException("网关：微信Page支付preCreate发生异常!");
            }
        } catch (Exception e) {
            log.error("微信用户扫商家统一下单创建失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("微信商家扫用户统一下单创建失败！");
        }
        return tradingVo;
    }

}