package com.itheima.easy.handler.common;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;

import com.itheima.easy.constant.SuperConstant;
import com.itheima.easy.constant.TradingConstant;
import com.itheima.easy.enums.TradingEnum;
import com.itheima.easy.exception.ProjectException;
import com.itheima.easy.handler.CommonPayHandler;
import com.itheima.easy.utils.ExceptionsUtil;

import com.itheima.easy.utils.SnowflakeIdWorker;
import com.itheima.easy.vo.RefundRecordVo;
import com.itheima.easy.vo.TradingVo;
import com.itheima.easy.wechat.client.Config;
import com.itheima.easy.wechat.client.Factory;
import com.itheima.easy.wechat.response.CloseResponse;
import com.itheima.easy.wechat.response.QueryResponse;
import com.itheima.easy.wechat.response.RefundResponse;
import com.itheima.easy.wechat.response.TradebillResponse;
import com.itheima.easy.wechat.utils.ResponseChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 支付基础功能-微信实现
 *
 * @Author itcast
 * @Create 2023/6/7
 **/
@Service
@Slf4j
public class WechatCommonPayHandler implements CommonPayHandler {

    @Autowired
    private Config config;

    /***
     * @description 统一收单线下交易查询
     * 该接口提供所有支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
     * @param tradingVo 交易单
     * @return
     */
    public TradingVo queryTrading(TradingVo tradingVo) {
        Factory.setOptions(config);

        try {
            //调用SDK
            QueryResponse queryResponse = Factory.Common().query(String.valueOf(tradingVo.getTradingOrderNo()));

            //判断响应是否成功
            boolean success = ResponseChecker.success(queryResponse);
            //分析交易状态
            boolean flag = true;
            if(success){
                //SUCCESS：支付成功,REFUND：转入退款,NOTPAY：未支付,CLOSED：已关闭,REVOKED：已撤销（仅付款码支付会返回）
                //USERPAYING：用户支付中（仅付款码支付会返回）,PAYERROR：支付失败（仅付款码支付会返回）
                switch (queryResponse.getTradeState()){
                    case TradingConstant.WECHAT_TRADE_CLOSED:
                        tradingVo.setTradingState(TradingConstant.TRADE_CLOSED);break;
                    case TradingConstant.WECHAT_TRADE_REVOKED:
                        tradingVo.setTradingState(TradingConstant.TRADE_CLOSED);break;
                    case TradingConstant.WECHAT_TRADE_SUCCESS:
                        tradingVo.setTradingState(TradingConstant.TRADE_SUCCESS);break;
                    case TradingConstant.WECHAT_TRADE_REFUND:
                        tradingVo.setTradingState(TradingConstant.TRADE_SUCCESS);break;
                    default:
                        flag = false;break;
                }
                if (flag){
                    tradingVo.setResultCode(queryResponse.getTradeState());
                    tradingVo.setResultMsg(queryResponse.getTradeStateDesc());
                    tradingVo.setResultJson(JSONObject.toJSONString(queryResponse));
                    return tradingVo;
                }else {
                    log.info("查询微信交易单：{},结果：{}", tradingVo.getTradingOrderNo(),queryResponse.getTradeState());
                }
            }
        } catch (Exception e) {
            log.error("查询微信统一下单失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("查询微信统一下单失败！");
        }
        return tradingVo;
    }

    /***
     * @description 统一收单交易退款接口
     * 当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，
     * 将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     * @param tradingVo 交易单
     * @return
     */
    public TradingVo refundTrading(TradingVo tradingVo) {
        Factory.setOptions(config);

        //生成退款请求编号
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1,1);
        tradingVo.setOutRequestNo(String.valueOf(snowflakeIdWorker.nextId()));

        try {
            //调用SDK
            RefundResponse response = Factory.Common().refund((String.valueOf(tradingVo.getTradingOrderNo())),
                    String.valueOf(tradingVo.getOperTionRefund()),
                    tradingVo.getOutRequestNo(), String.valueOf(tradingVo.getOperTionRefund()));

            //判断响应是否成功
            boolean success = ResponseChecker.success(response);

            if(success){
                tradingVo.setIsRefund(SuperConstant.YES);
            }else {
                log.error("统一下单退款交易失败");
            }
        } catch (Exception e) {
            log.error("微信统一下单退款失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("统一下单退款交易失败");
        }
        return tradingVo;
    }

    /***
     * @description 统一收单交易退款查询接口
     * 当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，
     * 将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     * @param refundRecordVo 退款交易单
     * @return
     */
    public RefundRecordVo queryRefundTrading(RefundRecordVo refundRecordVo) {
        Factory.setOptions(config);
        try {
            RefundResponse refundResponse = Factory.Common().queryRefund(refundRecordVo.getRefundNo());
            boolean success = ResponseChecker.success(refundResponse);
            if(success){
                refundRecordVo.setRefundStatus(TradingConstant.REFUND_STATUS_SUCCESS);
            }else {
                log.error("查询微信统一下单退款失败");
            }
            refundRecordVo.setRefundCode(refundResponse.getCode());
            refundRecordVo.setRefundMsg(refundResponse.getMessage());
        } catch (Exception e) {
            log.error("查询微信统一下单退款失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw new RuntimeException("查询微信统一下单退款失败！");
        }
        return refundRecordVo;
    }

    /***
     * @description 统一关闭订单
     * 1、商户订单支付失败需要生成新单号重新发起支付，要对原订单号调用关单，避免重复支付；
     * 2、系统下单后，用户支付超时，系统退出不再受理，避免用户继续，请调用关单接口。
     * @param tradingVo 交易单
     * @return
     */
    public TradingVo closeTrading(TradingVo tradingVo) {
        Factory.setOptions(config);

        try {
            CloseResponse response = Factory.Common().close(String.valueOf(tradingVo.getTradingOrderNo()));
            boolean success = ResponseChecker.success(response);
            if(success){
                tradingVo.setTradingState(TradingConstant.TRADE_CLOSED);
                return tradingVo;
            }else {
                log.error("微信关闭订单失败");
            }
        } catch (Exception e) {
            log.error("微信关闭订单失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw  new RuntimeException("微信关闭订单失败");
        }
        return tradingVo;
    }

    /***
     * @description 为方便商户快速查账，支持商户通过本接口获取商户离线账单下载地址
     * @param tradingVo 交易单
     * @return
     */
    public TradingVo downLoadBill(TradingVo tradingVo) {
        Factory.setOptions(config);

        try {
            TradebillResponse response = Factory.Common().downLoadTradebill(DateUtil.formatDate(tradingVo.getBillDate()));
            boolean success = ResponseChecker.success(response);
            if(success){
                tradingVo.setBillDownloadUrl(response.getDownloadUrl());
            }else {
                log.error("微信交易账单获取失败");
                tradingVo.setResultCode(response.getCode());
                tradingVo.setResultMsg(response.getMessage());
            }
        } catch (Exception e) {
            log.error("微信交易账单获取失败：{}", ExceptionsUtil.getStackTraceAsString(e));
            throw  new RuntimeException("微信交易账单获取失败");
        }
        return tradingVo;
    }
}
