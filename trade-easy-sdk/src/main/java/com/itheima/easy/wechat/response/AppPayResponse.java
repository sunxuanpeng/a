package com.itheima.easy.wechat.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * App支付返回结果封装
 */
@Data
@NoArgsConstructor
public class AppPayResponse extends BasicResponse{

    @JSONField(name="prepay_id")
    private String prepayId;

}
