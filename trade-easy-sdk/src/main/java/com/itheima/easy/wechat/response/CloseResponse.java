package com.itheima.easy.wechat.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 关闭订单返回结果
 */
@Data
@NoArgsConstructor
public class CloseResponse extends BasicResponse {

}
