package com.itheima.easy.wechat.client;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 微信支付相关配置
 **/
@Slf4j
@Data
@Builder
public class Config {
    //appId
    private String appid;

    //商户号
    private String mchId;

    //私钥字符串
    private String privateKey;

    //商户证书序列号
    private String mchSerialNo;

    //V3密钥
    private String apiV3Key;

    //请求地址 api.mch.weixin.qq.com
    private String domain;

    //回调地址
    private String notifyUrl;

}
