package com.itheima.easy.wechat.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 查询交易单返回结果封装
 *
 * @Author itcast
 * @Create 2023/6/7
 **/
@Data
public class TradebillResponse extends BasicResponse{
    @JSONField(name="download_url")
    private String downloadUrl;

    @JSONField(name="hash_type")
    private String hashType;

    @JSONField(name="hash_value")
    private String hashValue;
}
