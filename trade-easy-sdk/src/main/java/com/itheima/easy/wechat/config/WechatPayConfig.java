package com.itheima.easy.wechat.config;


import com.itheima.easy.wechat.client.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 微信支付配置类
 *
 * @Author itcast
 * @Create 2023/6/2
 **/
@Configuration
@EnableConfigurationProperties(WechatPayProperties.class)
public class WechatPayConfig {

    @Autowired
    WechatPayProperties wechatPayProperties;

    /***
     * @description 获得配置
     * @return
     */
    @Bean
    public Config config(){
        return Config.builder()
                .appid(wechatPayProperties.getAppid())
                .domain(wechatPayProperties.getDomain())
                .mchId(wechatPayProperties.getMchId())
                .mchSerialNo(wechatPayProperties.getMchSerialNo())
                .apiV3Key(wechatPayProperties.getApiV3Key())
                .privateKey(wechatPayProperties.getPrivateKey())
                .notifyUrl(wechatPayProperties.getNotifyUrl())
                .build();
    }
}
