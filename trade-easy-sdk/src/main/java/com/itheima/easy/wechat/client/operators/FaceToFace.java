package com.itheima.easy.wechat.client.operators;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.itheima.easy.utils.EmptyUtil;
import com.itheima.easy.wechat.client.Config;
import com.itheima.easy.wechat.client.WechatPayHttpClient;
import com.itheima.easy.wechat.response.PreCreateResponse;

import java.math.BigDecimal;

/**
 * 当面付
 *
 * @Author itcast
 * @Create 2023/6/2
 **/
public class FaceToFace {
    private Config config;

    public FaceToFace(Config config){
        this.config = config;
    }

    /**
     * 用户扫商家，生成收款二维码支付链接
     * @param outTradeNo
     * @param amount
     * @param description
     * @return
     * @throws Exception
     */
    public PreCreateResponse precreate(String outTradeNo, String amount, String description) throws Exception{
        //微信接口地址
        String uri = "/v3/pay/transactions/native";
        WechatPayHttpClient wechatPayHttpClient = WechatPayHttpClient.builder()
                .mchId(config.getMchId())
                .apiV3Key(config.getApiV3Key())
                .domain(config.getDomain() + uri)
                .mchSerialNo(config.getMchSerialNo())
                .privateKey(config.getPrivateKey())
                .build();

        //业务数据的封装
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode bodyParams = objectMapper.createObjectNode();
        BigDecimal bigDecimal = new BigDecimal(amount);
        BigDecimal multiply = bigDecimal.multiply(new BigDecimal(100));
        bodyParams.put("mchid",config.getMchId())
                .put("appid",config.getAppid() )
                .put("description", description)
                .put("notify_url", config.getNotifyUrl())
                .put("out_trade_no", outTradeNo);
        bodyParams.putObject("amount")
                .put("total", multiply.intValue());

        String body = wechatPayHttpClient.doPost(bodyParams);

        PreCreateResponse preCreateResponse = JSONObject.parseObject(body, PreCreateResponse.class);
        if(!EmptyUtil.isNullOrEmpty(preCreateResponse.getCodeUrl())){
            preCreateResponse.setCode("200");
            preCreateResponse.setMessage("网关请求成功");
            preCreateResponse.setSubCode("10000");
            preCreateResponse.setSubMessage("FaceToFace支付CodeUrl生成成功");
        }

        return preCreateResponse;
    }
}
