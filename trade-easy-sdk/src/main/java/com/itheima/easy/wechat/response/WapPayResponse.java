package com.itheima.easy.wechat.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 手机网页支付返回结果封装
 */
@Data
@NoArgsConstructor
public class WapPayResponse extends BasicResponse {

    @JSONField(name="prepay_id")
    private String prepayId;

}
