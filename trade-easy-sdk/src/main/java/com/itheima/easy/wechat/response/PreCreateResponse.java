package com.itheima.easy.wechat.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 当面付（native）返回结果封装
 */
@Data
@NoArgsConstructor
public class PreCreateResponse extends BasicResponse{

    //二维码请求地址
    @JSONField(name="code_url")
    private String codeUrl;

}
