package com.itheima.easy.wechat.client;


import com.itheima.easy.handler.common.Common;
import com.itheima.easy.wechat.client.operators.App;
import com.itheima.easy.wechat.client.operators.FaceToFace;
import com.itheima.easy.wechat.client.operators.Page;
import com.itheima.easy.wechat.client.operators.Wap;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 微信支付工厂类
 *
 * @Author itcast
 * @Create 2023/6/2
 **/
@Data
@Slf4j
public class Factory {

    private static Config config;

    public static void setOptions(Config config) {
        Factory.config = config;
    }

    public static FaceToFace FaceToFace(){
        return new FaceToFace(config);
    }

    public static Page Page(){
        return new Page(config);
    }

    public static Wap Wap(){
        return new Wap(config);
    }

    public static App App(){
        return new App(config);
    }

    public static Common Common(){
        return new Common(config);
    }
}
